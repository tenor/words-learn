import { Component, OnInit } from '@angular/core';
import { DictionaryService } from '../dictionary.service';
import { DictionaryComponent } from '../dictionary/dictionary.component';
import { Word } from '../word';

@Component({
  selector: 'app-add-words',
  templateUrl: './add-words.component.html',
  styleUrls: ['./add-words.component.css']
})

export class AddWordsComponent implements OnInit {
  newWord: Word = new Word(99, '', '');

  constructor(public dictService: DictionaryService) { }

  ngOnInit() {}

  addWord(word: string, trans: string): void {
    this.dictService.addWord(44, word, trans);
  }

}
