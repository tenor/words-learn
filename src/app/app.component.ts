import { Component, OnInit } from '@angular/core';
import { DictionaryService } from './dictionary.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'words-learn';
  constructor(public dictService: DictionaryService) { }

  ngOnInit() {
    this.dictService.initFullList();
  }
}
