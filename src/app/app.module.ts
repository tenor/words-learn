import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FormsModule } from '@angular/forms';

import { InMemoryDataService } from './in-memory-data.service';

import { AppComponent } from './app.component';
import { DictionaryComponent } from './dictionary/dictionary.component';
import { AddWordsComponent } from './add-words/add-words.component';
import { TestWordsComponent } from './test-words/test-words.component';
import { DictionaryService } from './dictionary.service';

const appRoutes: Routes = [
  {path: 'dictionary', component: DictionaryComponent},
  {path: 'add-words', component:  AddWordsComponent},
  {path: 'test-words', component: TestWordsComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    DictionaryComponent,
    AddWordsComponent,
    TestWordsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: false}) // <-- debugging purposes only
  ],
  providers: [DictionaryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
