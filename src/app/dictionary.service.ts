import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Word } from './word';
import { InMemoryDataService } from './in-memory-data.service';

@Injectable({
  providedIn: 'root'
})

export class DictionaryService {
  private _fullList: Word[] = [];
  private _dictionary: Word[]; // chosen list for learning
  private _config = {
    words: 6,
    selection: 'last', // first, last, random, checked, fromto
    varNum: 5, // variants of answers
    goodLineMax: 7
  };

  constructor(
    private http: HttpClient,
    private inMemoryDataService: InMemoryDataService
  ) { }

  initFullList(): void {
    const tmpList: any[] = this.inMemoryDataService.createDb().dictionary;
    for (let i = 0; i < tmpList.length; i++) {
      this._fullList.push(new Word(tmpList[i].id, tmpList[i].word, tmpList[i].trans));
    }
  }

  get fullList(): Word[] {
    if (this._fullList.length < 1) {
      this.initFullList();
    }
    return this._fullList;
  }

  get config() {
    return this._config;
  }

  generateDictionary(): Word[] {
    switch (this.config.selection) {
      case 'first':
        this._dictionary = this._fullList.slice(0, this.config.words);
        break;
      case 'last':
        this._dictionary = this._fullList.slice(-this.config.words);
        break;
      case 'rand': break;
    }
    return this._dictionary;
  }

  get dictionary(): Word[] {
    if (!this._dictionary) {
      this.generateDictionary();
    }
    return this._dictionary;
  }

  getFilteredDictionary(): Word[] {
    const fdict: Word[] = [];
    if (!this._dictionary) {
      this.generateDictionary();
      return this._dictionary;
    }
    let i = 0;
    while (i < this._dictionary.length) {
      // if word not learned yet
      if (this._dictionary[i].goodLine < this.config.goodLineMax) {
          fdict.push(this._dictionary[i]);
      }
      i++;
    }
    return fdict;
  }

  getVariants(tr: string): string[] {
    const vars: string[] = [];
    const len: number = this._fullList.length;
    const tri: number = Math.floor(Math.random() * (this._config.varNum)); // translation random index
    let ri: number; // random index for fullList
    let i = 0; // index for variznts
    while (i < this._config.varNum) {
      if (i === tri) {
        vars.push(tr);
        i++;
      } else {
        ri = Math.floor(Math.random() * len);
        if (this._fullList[ri].trans !== tr) {
          if (vars.indexOf(this._fullList[ri].trans) === -1) {
            vars.push(this._fullList[ri].trans);
            i++;
          }
        }
      }
    }
    this.shuffle(vars);
    return vars;
  }

  addWord(id: number, word: string, trans: string): void {
    if (!this._fullList) {
      this.initFullList();
    }
    const nw: Word = new Word(id, word, trans);
    this._fullList.push(nw);
  }

  checkLearn(tested: Word): void {
    if (tested.goodLine >= this.config.goodLineMax) {
      tested.learned = 'learned';
    }
  }

  shuffle(array: any[]): any[] {
    let counter = array.length;
    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        const index: number = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        const temp: any[] = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }
    return array;
  }
}
