import { Component, OnInit } from '@angular/core';
import { Word } from '../word';
import { DictionaryService } from '../dictionary.service';

@Component({
  selector: 'app-dictionary',
  templateUrl: './dictionary.component.html',
  styleUrls: ['./dictionary.component.css']
})

export class DictionaryComponent implements OnInit {
  dictionary: Word[];
  config = {};
  constructor(public dictService: DictionaryService) { }

  ngOnInit(): void {
    this.config = this.dictService.config;
    this.dictionary = this.dictService.dictionary;
  }

  generateDictionary(): void {
    this.dictionary = this.dictService.generateDictionary();
  }
}
