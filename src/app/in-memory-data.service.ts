// temp data source and service for words-learn project
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Word } from './word';

import { Injectable } from '@angular/core'; // delete later

@Injectable({
  providedIn: 'root'
})

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const dictionary = [
      { id: 1, word: 'wall', trans: 'стена' },
      { id: 2, word: 'weather', trans: 'погода' },
      { id: 3, word: 'home', trans: 'дом' },
      { id: 4, word: 'father', trans: 'отец' },
      { id: 5, word: 'bring', trans: 'приносить' },
      { id: 6, word: 'angular', trans: 'угловатый' },
      { id: 7, word: 'react', trans: 'реагировать' },
      { id: 8, word: 'visibility', trans: 'видимость' },
    ];
    return {dictionary};
  }

  genId(dictionary: Word[]): number {
    return dictionary.length > 0 ? Math.max(...dictionary.map(word => word.id)) + 1 : 11;
  }
}
