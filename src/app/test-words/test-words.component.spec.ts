import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestWordsComponent } from './test-words.component';

describe('TestWordsComponent', () => {
  let component: TestWordsComponent;
  let fixture: ComponentFixture<TestWordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestWordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestWordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
