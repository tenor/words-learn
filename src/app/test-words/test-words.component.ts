import { Component, OnInit } from '@angular/core';
import { DictionaryService } from '../dictionary.service';
import { Word } from '../word';

@Component({
  selector: 'app-test-words',
  templateUrl: './test-words.component.html',
  styleUrls: ['./test-words.component.css']
})

export class TestWordsComponent implements OnInit {
  target = null;
  firstGood = true;
  dictIndex = -1;
  tested: Word; // current tested word
  transVar: string[] = []; // translation variants

  constructor(public dictService: DictionaryService) { }

  ngOnInit() {
   this.nextWord();
  }
  // take next random not learned word from the dictionary Array
  nextWord() {
    // this is your first good answer
    this.firstGood = true;
    if (this.target) {
      this.target.classList.remove('goods');
    }
    this.dictIndex = Math.floor(Math.random() * this.dictService.getFilteredDictionary().length);
     this.tested = this.dictService.getFilteredDictionary()[this.dictIndex];
    this.transVar = this.dictService.getVariants(this.tested.trans);
  }

  checkWord(trans: string): void {
    if (this.tested.trans === trans) {
      if (this.firstGood === true) {
        this.tested.goodAnswersNum++;
        this.tested.goodLine++;
        this.firstGood = false;
        this.dictService.checkLearn(this.tested);
      }
    } else {
      this.tested.badAnswersNum++;
      this.tested.goodLine = 0;
    }
  }
}
